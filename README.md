If you want to help translate mineland network into your language or improve the current translation, then our project is happy to provide you with such an opportunity.

The source language of the project is Russian, all Russian messages are in the [ru.yml](https://gitlab.com/mineland-community/mineland-localization/-/blob/master/ru.yml) file.  
But the project is well translated into English, all English messages are presented in the [en.yml](https://gitlab.com/mineland-community/mineland-localization/-/blob/master/en.yml) file.

To translate a project into your language, you need:
1. Create a file with the name YOUR_LANGUAGE.yml, the name of the language must correspond to its two letter code in lower case.
2. Translate messages or part of messages using any other language file as a basis.  
Important: do not translate message keys, they are written BEFORE the `:` sign.
3. Create a merge request with your language file.
4. Your request will be checked by an expert and, if all is well, will add your translation to the server and it will be available to all players.

If the file lacks any messages, then the messages will be automatically taken from other languages ​​where the message translations are available.

You can also improve the translation of current messages, correct typos and errors, just make messages more concise and beautiful, such edits will also be approved.

If you do not want to change the translation files yourself, but want to report a translation error, please create an [Issue](https://gitlab.com/mineland-community/mineland-localization/-/issues/new), an expert will take care of your question.

If you yourself want to contribute a lot to the translation
 mineland Mineland Network and become an expert yourself, then write to our support https://mineland.net/discord/en
Perhaps we will give you access to this gitlab project.
